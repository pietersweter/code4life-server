﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Core
{
    public enum Result
    {
        None = 0,
        Passed = 1,
        Failed = -1
    }
}
