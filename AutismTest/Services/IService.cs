﻿using AutismTest.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Services
{
    public interface IService
    {
        Test GetTestByUserId(string userId);
        IList<Subquestion> GetSubquestionsByQuestionId(int id);
        TestExercise GetTestExercise(int testId, int exerciseId);
        IList<Tag> GetTagsByExerciseId(int exerciseId);
        IList<TestExercise> GetTestExercisesByTestId(int testId);
        IList<TestExercise> GetTestExerciseWithoutTestId(int testId);
    }
}
