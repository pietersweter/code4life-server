﻿using AutismTest.Domain;
using AutismTest.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AutismTest.Data
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly AppDbContext _db;
        private DbSet<T> _table;
        private DbSet<T> Table
        {
            get
            {
                if (_table == null)
                {
                    _table = _db.Set<T>();
                }

                return _table;
            }
        }

        public Repository(AppDbContext db)
        {
            _db = db;
        }

        public T GetById(int id)
        {
            return Table.Find(id);
        }

        public IList<T> GetAll()
        {
            return Table.ToList();
        }

        public void Insert(T entity)
        {
            if (entity != null)
            {
                Table.Add(entity);
                _db.SaveChanges();
            }
            else
            {
                throw new ArgumentNullException("Entity is null");
            }
        }

        public void Update(T entity)
        {
            if (entity != null)
            {
                _db.SaveChanges();
            }
            else
            {
                throw new ArgumentNullException("Entity is null");
            }
        }

        public void Delete(T entity)
        {
            if (entity != null)
            {
                _db.Remove(entity);
                _db.SaveChanges();
            }
            else
            {
                throw new ArgumentNullException("Entity is null");
            }
        }

        public bool Exists(int id)
        {
            var foundEntity = GetById(id);
            return (foundEntity != null); 
        }
    }
}
