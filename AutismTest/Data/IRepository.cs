﻿using AutismTest.Domain;
using AutismTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Data
{
    public interface IRepository<T> where T : IBaseEntity
    {
        T GetById(int id);
        IList<T> GetAll();
        void Insert(T entity);
        void Update(T entity);
        void Delete(T entity);
        bool Exists(int id);
    }
}
