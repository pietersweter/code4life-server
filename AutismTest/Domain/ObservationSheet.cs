﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Domain
{
    public class ObservationSheet : BaseEntity
    {
        public DateTime ObservationDate{ get; set; }
        public String Description { get; set; }
        public int CorrespondingQuestionId { get; set; }
        public string VideoPath { get; set; }
    }
}
