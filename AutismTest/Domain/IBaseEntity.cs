﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Domain
{
    public interface IBaseEntity
    {
        int Id { get; set; }
    }
}
