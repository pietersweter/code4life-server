﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Domain
{
    public class BaseEntity : IBaseEntity
    {
        public int Id { get; set; }
    }
}
