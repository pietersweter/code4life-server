﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Domain
{
    public class Question : BaseEntity
    {
        public string Body { get; set; }
        public int Threshold { get; set; }
        public int ThresholdPassQuestionId { get; set; }
        public int ThresholdFailQuestionId { get; set; }
        public int SecondaryThreshold { get; set; }
        public int SecondaryThresholdQuestionId { get; set; }
    }
}
