﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Domain
{
    public class Tag : BaseEntity
    {
        public int ExerciseId { get; set; }
        public string Name { get; set; }
    }
}
