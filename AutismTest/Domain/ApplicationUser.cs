﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutismTest.Core;
using Microsoft.AspNetCore.Identity;

namespace AutismTest.Domain
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public bool IsAdmin { get; set; }
    }
}
