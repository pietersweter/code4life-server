﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Domain
{
    public class Exercise : BaseEntity
    {
        public string Description { get; set; }
        public int FirstQuestionId { get; set; }
        public int Order { get; set; }
        public string ImageUrl { get; set; }
    }
}
