﻿using AutismTest.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Models
{
    public class ExerciseParentPageModel
    {
        public string Email { get; set; }
        public int TestId { get; set; }
        public IList<ExerciseModel> Exercises { get; set; }
    }
}
