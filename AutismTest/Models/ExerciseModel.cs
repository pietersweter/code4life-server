﻿using AutismTest.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutismTest.Models
{
    public class ExerciseModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int FirstQuestionId { get; set; }
        public int Order { get; set; }
        public string ImageUrl { get; set; }
        public Result Result { get; set; }
        public IList<TagModel> Tags { get; set; }
    }
}
