﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutismTest.Core;
using AutismTest.Data;
using AutismTest.Domain;
using AutismTest.Models;
using AutismTest.Services;
using Microsoft.AspNetCore.Identity;

namespace AutismTest
{
    public class Factory : IFactory
    {
        private readonly IRepository<Exercise> _exerciseRepository;
        private readonly IRepository<TestExercise> _testExerciseRepository;
        private readonly IRepository<Question> _questionRepository;
        private readonly IService _service;

        public Factory(
            IRepository<Exercise> exerciseRepository,
            IRepository<TestExercise> testExerciseRepository,
            IService service,
            IRepository<Question> questionRepository)
        {
            _exerciseRepository = exerciseRepository;
            _testExerciseRepository = testExerciseRepository;
            _service = service;
            _questionRepository = questionRepository;
        }

        public ExerciseParentPageModel CreateExercisePageModel(ApplicationUser user, Test test)
        {
            var model = new ExerciseParentPageModel
            {
                Email = user.Email,
                TestId = test.Id,
                Exercises = _exerciseRepository.GetAll().Select(exercise => new ExerciseModel
                {
                    Id = exercise.Id,
                    Description = exercise.Description,
                    FirstQuestionId = exercise.FirstQuestionId,
                    ImageUrl = exercise.ImageUrl,
                    Order = exercise.Order,
                    Result = _service.GetTestExercise(test.Id, exercise.Id)?.Result ?? Result.None,
                    Tags = _service.GetTagsByExerciseId(exercise.Id).Select(tag => new TagModel
                    {
                        Id = tag.Id,
                        ExerciseId = tag.ExerciseId,
                        Name = tag.Name
                    }).ToList()
                }).ToList()
            };

            return model;
        }

        public QuestionModel CreateQuestionModel(Question question)
        {
            var model = new QuestionModel
            {
                Id = question.Id,
                Body = question.Body,
                Threshold = question.Threshold,
                ThresholdPassQuestionId = question.ThresholdPassQuestionId,
                ThresholdPassQuestion = TryToGetNextQuestion(question.ThresholdPassQuestionId),
                ThresholdFailQuestionId = question.ThresholdFailQuestionId,
                ThresholdFailQuestion = TryToGetNextQuestion(question.ThresholdFailQuestionId),
                SecondaryThreshold = question.Threshold,
                SecondaryThresholdQuestionId = question.SecondaryThresholdQuestionId,
                SecondaryThresholdQuestion = TryToGetNextQuestion(question.SecondaryThresholdQuestionId),
                Subquestions = _service.GetSubquestionsByQuestionId(question.Id).Select(subquestion => new SubquestionModel
                {
                    Id = subquestion.Id,
                    Body = subquestion.Body,
                    Order = subquestion.Order,
                    QuestionId = subquestion.QuestionId
                }).ToList()
            };

            return model;
        }

        public IList<UserModel> CreateUserModel(UserManager<ApplicationUser> userManager)
        {
            var model = userManager.Users.ToList().Select(user =>
            {
                var testId = _service.GetTestByUserId(user.Id)?.Id ?? 0;

                return new UserModel
                {
                    Id = user.Id,
                    Email = user.Email,
                    TestId = testId,
                    Score = _testExerciseRepository.GetAll().Where(te => te.TestId == testId && te.Result == Result.Passed).Count(),
                    ExerciseCount = _exerciseRepository.GetAll().Count
                };
            }).ToList();

            return model;
        }

        public IList<ExerciseModel> CreateTestSummary(int testId)
        {
            var model = _exerciseRepository.GetAll().Select(exercise => new ExerciseModel
            {
                Id = exercise.Id,
                Description = exercise.Description,
                ImageUrl = exercise.ImageUrl,
                Result = _service.GetTestExercise(testId, exercise.Id)?.Result ?? Result.None,
            }).ToList();

            return model;
        }

        private QuestionModel TryToGetNextQuestion(int id)
        {
            QuestionModel questionModel = null;

            if (PointsToNextQuestion(id))
            {
                var nextQuestion = _questionRepository.GetById(id);
                if (nextQuestion != null)
                {
                    questionModel = CreateQuestionModel(nextQuestion);
                }
            }

            return questionModel;
        }

        private bool PointsToNextQuestion(int questionId)
        {
            if (questionId != (int)QuestionAnswer.Fail && questionId != (int)QuestionAnswer.Pass)
            {
                return true;
            }

            return false;
        }
    }
}
