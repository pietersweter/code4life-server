﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutismTest.Data;
using AutismTest.Models;
using AutismTest.Domain;
using AutismTest.Services;

namespace AutismTest.Controllers
{
    [Produces("application/json")]
    [Route("api/ObservationSheet")]
    public class ObservationSheetsController : Controller
    {
        private readonly IRepository<ObservationSheet> _observationSheetRepository;

        public ObservationSheetsController(IRepository<ObservationSheet> observationSheetRepository)
        {
            _observationSheetRepository = observationSheetRepository;
        }

        // GET: api/ObservationSheets
        [HttpGet]
        public IEnumerable<ObservationSheet> GetObservationSheets()
        {
            return _observationSheetRepository.GetAll();
        }

        // GET: api/ObservationSheets/5
        [HttpGet("{id}")]
        public IActionResult GetObservationSheet([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var observationSheet = _observationSheetRepository.GetById(id);

            if (observationSheet == null)
            {
                return NotFound();
            }

            return Ok(observationSheet);
        }

        // PUT: api/ObservationSheets/5
        [HttpPut("{id}")]
        public IActionResult PutObservationSheet([FromRoute] int id,[FromBody] ObservationSheet observationSheet)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != observationSheet.Id)
            {
                return BadRequest();
            }

            try
            {
                _observationSheetRepository.Update(observationSheet);
            }


            catch (DbUpdateConcurrencyException)
            {
                if (!ObservationSheetExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ObservationSheets
        [HttpPost]
        public IActionResult PostObservationSheet([FromBody] ObservationSheet observationSheet)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _observationSheetRepository.Insert(observationSheet);

            return CreatedAtAction("GetObservationSheet", new { id = observationSheet.Id }, observationSheet);
        }

        // DELETE: api/ObservationSheets/5
        [HttpDelete("{id}")]
        public IActionResult DeleteObservationSheet([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var observationSheet = _observationSheetRepository.GetById(id);
            if (observationSheet == null)
            {
                return NotFound();
            }

            _observationSheetRepository.Delete(observationSheet);

            return Ok(observationSheet);
        }

        private bool ObservationSheetExists(int id)
        {
            return _observationSheetRepository.Exists(id);
        }
    }
}