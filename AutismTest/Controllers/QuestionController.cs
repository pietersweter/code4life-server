﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutismTest.Data;
using AutismTest.Models;
using AutismTest.Domain;
using AutismTest.Services;

namespace AutismTest.Controllers
{
    [Produces("application/json")]
    [Route("api/Question")]
    public class QuestionController : Controller
    {
        private readonly IRepository<Question> _questionRepository;
        private readonly IService _service;
        private readonly IFactory _factory;


        public QuestionController(
            IRepository<Question> questionRepository, 
            IService service,
            IFactory factory)
        {
            _questionRepository = questionRepository;
            _service = service;
            _factory = factory;
        }

        // GET: api/Questions
        [HttpGet]
        public IEnumerable<Question> GetQuestions()
        {
            return _questionRepository.GetAll();
        }

        // GET: api/Questions/5
        [HttpGet("{id}")]
        public IActionResult GetQuestion([FromRoute] int id)
        {
            if (id == 0)
            {
                return BadRequest(ModelState);
            }

            var question = _questionRepository.GetById(id);
            if (question == null)
            {
                return NotFound();
            }

            var model = _factory.CreateQuestionModel(question);

            return Ok(model);
        }

        // PUT: api/Questions/5
        [HttpPut("{id}")]
        public IActionResult PutQuestion([FromRoute] int id,[FromBody] Question question)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != question.Id)
            {
                return BadRequest();
            }

            try
            {
                _questionRepository.Update(question);
            }


            catch (DbUpdateConcurrencyException)
            {
                if (!QuestionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Questions
        [HttpPost]
        public IActionResult PostQuestion([FromBody] Question question)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _questionRepository.Insert(question);

            return CreatedAtAction("GetQuestion", new { id = question.Id }, question);
        }

        // DELETE: api/Questions/5
        [HttpDelete("{id}")]
        public IActionResult DeleteQuestion([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var question = _questionRepository.GetById(id);
            if (question == null)
            {
                return NotFound();
            }

            _questionRepository.Delete(question);

            return Ok(question);
        }

        private bool QuestionExists(int id)
        {
            return _questionRepository.Exists(id);
        }

        [Route("GetSubquestionByQuestionId/{id}")]
        public IEnumerable<Subquestion> SubquestionsOfQuestion([FromRoute]int questionId)
        {
            return _service.GetSubquestionsByQuestionId(questionId);
        }
    }
}