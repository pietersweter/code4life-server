﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutismTest.Data;
using AutismTest.Models;
using AutismTest.Domain;
using AutismTest.Services;

namespace AutismTest.Controllers
{
    [Produces("application/json")]
    [Route("api/MedicalInstitution")]
    public class MedicalInstitutionsController : Controller
    {
        private readonly IRepository<MedicalInstitution>  _medicalInstitutionRepository;

        public MedicalInstitutionsController(IRepository<MedicalInstitution> medicalInstitutionRepository)
        {
            _medicalInstitutionRepository = medicalInstitutionRepository;
        }

        // GET: api/MedicalInstitutions
        [HttpGet]
        public IEnumerable<MedicalInstitution> GetMedicalInstitutions()
        {
            return _medicalInstitutionRepository.GetAll();
        }

        // GET: api/MedicalInstitutions/5
        [HttpGet("{id}")]
        public IActionResult GetMedicalInstitution([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicalInstitution = _medicalInstitutionRepository.GetById(id);

            if (medicalInstitution == null)
            {
                return NotFound();
            }

            return Ok(medicalInstitution);
        }

        // PUT: api/MedicalInstitutions/5
        [HttpPut("{id}")]
        public IActionResult PutMedicalInstitution([FromRoute] int id,[FromBody] MedicalInstitution medicalInstitution)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != medicalInstitution.Id)
            {
                return BadRequest();
            }

            try
            {
                _medicalInstitutionRepository.Update(medicalInstitution);
            }


            catch (DbUpdateConcurrencyException)
            {
                if (!MedicalInstitutionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MedicalInstitutions
        [HttpPost]
        public IActionResult PostMedicalInstitution([FromBody] MedicalInstitution medicalInstitution)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _medicalInstitutionRepository.Insert(medicalInstitution);

            return CreatedAtAction("GetMedicalInstitution", new { id = medicalInstitution.Id }, medicalInstitution);
        }

        // DELETE: api/MedicalInstitutions/5
        [HttpDelete("{id}")]
        public IActionResult DeleteMedicalInstitution([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicalInstitution = _medicalInstitutionRepository.GetById(id);
            if (medicalInstitution == null)
            {
                return NotFound();
            }

            _medicalInstitutionRepository.Delete(medicalInstitution);

            return Ok(medicalInstitution);
        }

        private bool MedicalInstitutionExists(int id)
        {
            return _medicalInstitutionRepository.Exists(id);
        }
    }
}