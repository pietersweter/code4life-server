﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutismTest.Data;
using AutismTest.Models;
using AutismTest.Domain;
using AutismTest.Services;
using Microsoft.AspNetCore.Identity;

namespace AutismTest.Controllers
{
    [Produces("application/json")]
    [Route("api/Exercise")]
    public class ExerciseController : Controller
    {
        private readonly IRepository<Exercise>  _exerciseRepository;
        private readonly IFactory _factory;
        private readonly IRepository<Test> _testRepository;
        private readonly IService _service;
        private readonly UserManager<ApplicationUser> _userManager;

        public ExerciseController(
            IRepository<Exercise> exerciseRepository,
            IFactory factory,
            IRepository<Test> testRepository,
            IService service,
            UserManager<ApplicationUser> userManager)
        {
            _exerciseRepository = exerciseRepository;
            _factory = factory;
            _testRepository = testRepository;
            _service = service;
            _userManager = userManager;
        }

        // GET: api/Exercise
        [HttpGet]
        public IEnumerable<Exercise> GetExercises()
        {
            return _exerciseRepository.GetAll();
        }

        [HttpGet]
        [Route("GetExercisesByTestId/{testId}")]
        public async Task<IActionResult> GetExercisesByTestId([FromRoute] int testId)
        {
            var test = _testRepository.GetById(testId);
            if (test == null)
            {
                return BadRequest();
            }

            var user = await _userManager.FindByIdAsync(test.UserId);
            if (user == null)
            {
                return BadRequest();
            }

            var model = _factory.CreateExercisePageModel(user, test);

            return Ok(model);
        }

        // GET: api/Exercise/5
        [HttpGet("{id}")]
        public IActionResult GetExercise([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exercise = _exerciseRepository.GetById(id);

            if (exercise == null)
            {
                return NotFound();
            }

            return Ok(exercise);
        }

        // PUT: api/Exercise/5
        [HttpPut("{id}")]
        public IActionResult PutExercise([FromRoute] int id,[FromBody] Exercise exercise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != exercise.Id)
            {
                return BadRequest();
            }

            try
            {
                _exerciseRepository.Update(exercise);
            }


            catch (DbUpdateConcurrencyException)
            {
                if (!ExerciseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Exercise
        [HttpPost]
        public IActionResult PostExercise([FromBody] Exercise exercise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _exerciseRepository.Insert(exercise);

            return CreatedAtAction("GetExercise", new { id = exercise.Id }, exercise);
        }

        // DELETE: api/Exercise/5
        [HttpDelete("{id}")]
        public IActionResult DeleteExercise([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exercise = _exerciseRepository.GetById(id);
            if (exercise == null)
            {
                return NotFound();
            }

            _exerciseRepository.Delete(exercise);

            return Ok(exercise);
        }

        private bool ExerciseExists(int id)
        {
            return _exerciseRepository.Exists(id);
        }
    }
}