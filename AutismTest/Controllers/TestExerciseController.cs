﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutismTest.Data;
using AutismTest.Models;
using AutismTest.Core;
using AutismTest.Domain;
using AutismTest.Services;
using Microsoft.AspNetCore.Identity;

namespace AutismTest.Controllers
{
    [Produces("application/json")]
    [Route("api/TestExercise")]
    public class TestExercisesController : Controller
    {
        private readonly IRepository<TestExercise> _testExerciseRepository;
        private readonly IRepository<Test> _testRepository;
        private readonly IService _service;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IFactory _factory;

        public TestExercisesController(
            IRepository<TestExercise> testExerciseRepository,
            IRepository<Test> testRepository,
            IService service,
            UserManager<ApplicationUser> userManager,
            IFactory factory)
        {
            _testExerciseRepository = testExerciseRepository;
            _testRepository = testRepository;
            _service = service;
            _userManager = userManager;
            _factory = factory;
        }

        // GET: api/TestExercises
        [HttpGet]
        public IEnumerable<TestExercise> GetTestExercises()
        {
            return _testExerciseRepository.GetAll();
        }

        // GET: api/TestExercises/5
        [HttpGet("{id}")]
        public IActionResult GetTestExercise([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exercise = _testExerciseRepository.GetById(id);

            if (exercise == null)
            {
                return NotFound();
            }

            return Ok(exercise);
        }

        // PUT: api/TestExercises/5
        [HttpPut("{id}")]
        public IActionResult PutTestExercise([FromRoute] int id, [FromBody] TestExercise exercise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != exercise.Id)
            {
                return BadRequest();
            }

            try
            {
                _testExerciseRepository.Update(exercise);
            }


            catch (DbUpdateConcurrencyException)
            {
                if (!TestExerciseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TestExercises
        [HttpPost]
        public IActionResult PostTestExercise([FromBody] TestExercise exercise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _testExerciseRepository.Insert(exercise);

            return CreatedAtAction("GetTestExercise", new { id = exercise.Id }, exercise);
        }

        // DELETE: api/TestExercises/5
        [HttpDelete("{id}")]
        public IActionResult DeleteTestExercise([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exercise = _testExerciseRepository.GetById(id);
            if (exercise == null)
            {
                return NotFound();
            }

            _testExerciseRepository.Delete(exercise);

            return Ok(exercise);
        }

        private bool TestExerciseExists(int id)
        {
            return _testExerciseRepository.Exists(id);
        }

        [HttpGet]
        [Route("RateAutismProbability/{testId}")]
        public Decimal RateAutismProbability(int testId)
        {
            var testExercises = _service.GetTestExercisesByTestId(testId);
            var otherExercises = _service.GetTestExerciseWithoutTestId(testId);
            var confirmationNumber = 0;
            var denyNumber = 0;

            foreach (TestExercise testExercise in testExercises)
            {
                var otherTestExercisesWithTheSameExerciseId = otherExercises
                        .Where(otherExercise => otherExercise.ExerciseId == testExercise.ExerciseId && otherExercise.Result == testExercise.Result);

                var query = otherTestExercisesWithTheSameExerciseId
                    .Join(_testRepository.GetAll(),
                      otherTestExercise => otherTestExercise.TestId,
                      test => test.Id,
                      (otherTestExercise, test) => new { TestExercise = otherTestExercise, Test = test }).ToList();

                confirmationNumber += query.Count(test => test.Test.Result.Equals(Result.Passed));
                denyNumber += query.Count(test => test.Test.Result.Equals(Result.Failed));
            }

            var numberOfElements = confirmationNumber + denyNumber;
            return (confirmationNumber * 100 / (numberOfElements > 0 ? numberOfElements : 1));
        }

        [HttpPost]
        [Route("TestExerciseData")]
        public async Task<IActionResult> PostTestExerciseAndGetData([FromBody] TestExercise testExercise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _testExerciseRepository.Insert(testExercise);

            var test = _testRepository.GetById(testExercise.TestId);
            if (test == null)
            {
                return BadRequest();
            }

            var user = await _userManager.FindByIdAsync(test.UserId);
            if (user == null)
            {
                return BadRequest();
            }

            if (user.IsAdmin)
            {
                var model = _factory.CreateUserModel(_userManager);

                return Ok(model);
            }
            else
            {
                var model = _factory.CreateExercisePageModel(user, test);

                return Ok(model);
            }
        }
    }
}