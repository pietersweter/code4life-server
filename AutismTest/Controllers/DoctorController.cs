﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutismTest.Domain;
using AutismTest.Data;

namespace AutismTest.Controllers
{
    [Produces("application/json")]
    [Route("api/Doctor")]
    public class DoctorController : Controller
    {
        private readonly IRepository<Doctor> _doctorRepository;

        public DoctorController(IRepository<Doctor> doctorRepository)
        {
            _doctorRepository = doctorRepository;
        }

        // GET: api/Doctor
        [HttpGet]
        public IEnumerable<Doctor> GetDoctors()
        {
            return _doctorRepository.GetAll();
        }

        // GET: api/Doctor/5
        [HttpGet("{id}")]
        public IActionResult GetDoctor([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var doctor = _doctorRepository.GetById(id);

            if (doctor == null)
            {
                return NotFound();
            }

            return Ok(doctor);
        }

        // PUT: api/Doctor/5
        [HttpPut("{id}")]
        public IActionResult PutDoctor([FromRoute] int id, [FromBody] Doctor doctor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != doctor.Id)
            {
                return BadRequest();
            }

            try
            {
                _doctorRepository.Update(doctor);
            }


            catch (DbUpdateConcurrencyException)
            {
                if (!DoctorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Doctor
        [HttpPost]
        public IActionResult PostDoctor([FromBody] Doctor doctor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _doctorRepository.Insert(doctor);

            return CreatedAtAction("GetDoctor", new { id = doctor.Id }, doctor);
        }

        // DELETE: api/Doctor/5
        [HttpDelete("{id}")]
        public IActionResult DeleteMedicalInstitution([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicalInstitution = _doctorRepository.GetById(id);
            if (medicalInstitution == null)
            {
                return NotFound();
            }

            _doctorRepository.Delete(medicalInstitution);

            return Ok(medicalInstitution);
        }

        private bool DoctorExists(int id)
        {
            return _doctorRepository.Exists(id);
        }
    }
}